import React = require("react");
import { Phrase } from "../model/phrase.model";

export interface AppContextInterface {
    phrases:Phrase[]
}

const ctxt = React.createContext<AppContextInterface | null>(null);

export const AppContextProvider = ctxt.Provider;
  
export const AppContextConsumer = ctxt.Consumer;