import { Phrase } from '../model/phrase.model';
import * as React from 'react';
import { AppContextInterface } from './test.service';



export class PhraseService{
    private phrases = 
    [
        new Phrase(1,'Right of the Bat'),
        new Phrase(2,"Heads Up")
    ]

    getPhrases(){
        return this.phrases.slice();
     }

     addPhrases(item:Phrase){
        this.phrases.push(item);
        return this.phrases.slice();
    }

    deletePhrases(index:number){
        this.phrases.splice(index, 1);
        return this.phrases.slice();
    }

    getLength(){
        return this.phrases.length;
    }

    
}
