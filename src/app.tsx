import * as React from "react";
import {render} from "react-dom";
import {Router,Switch,Route} from "react-router";
import {BrowserRouter} from "react-router-dom";
import createBrowserHistory from 'history/createBrowserHistory'
import { AppContextProvider, AppContextInterface } from './service/test.service';


import { Edit } from './components/Edit';
import { Home } from './components/Home';
import { Header } from './components/Header';
import { Phrase } from "./model/phrase.model";
import { PhraseService } from './service/phrase.service';
import { Root } from './components/Root';

const newHistory = createBrowserHistory();

let phrase_obj = new PhraseService();
let phrases_context:AppContextInterface = {phrases:[
  new Phrase(1,'Right of the Bat'),
  new Phrase(2,"Heads Up")
]}

class App extends React.Component{
  render() {
    return(
      <AppContextProvider  value = {phrases_context}>
        <div>
          <Home Phrase={phrase_obj}/>
         </div>
      </AppContextProvider>
     
    );
  }
}

render(<App/>, window.document.getElementById('app'));