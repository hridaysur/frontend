import * as React from 'react';
import { PhraseService } from '../service/phrase.service';
import { Phrase } from '../model/phrase.model';
import { AppContextConsumer, AppContextInterface } from '../service/test.service';
import { Edit } from './Edit';
import Popup from './Popup';
import { Delete } from './Delete';



interface iProp{
    Phrase:PhraseService
}
var phr:Phrase[];
// ,{phrases:Phrase[]}
export class Home extends React.Component<iProp,{isOpen:boolean,phrases_t:AppContextInterface}>{
    constructor(props1:iProp){
        super(props1);
        
        this.state ={
            isOpen: false,
            phrases_t:{phrases:[]}
        }
    }

    openPopup = () => {
        this.setState({
          isOpen: true
        });
      }
    
      closePopup = () => {
        this.setState({
          isOpen: false
        });
      }
      deleteItem = (e:any) => {
          console.log(e);
          this.props.Phrase.deletePhrases(e);
          console.log(this.props.Phrase.getPhrases());
      }

        
    render(){   
        phr = this.props.Phrase.getPhrases();
        return(        
           <AppContextConsumer>
               {(appContext) => appContext && (
                <div className="container">
                    <div className="row">
                        <div className="col-xs-10">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Id</th>
                                    <th scope="col">Phrase</th>
                                    <th scope="col">#</th>
                                    </tr>
                                </thead>
                             
                             <tbody>
                              {phr.map((phrases,index) => {
                                 return <tr key={index}>
                                            <th scope="row">{phrases.id}</th>
                                            <td>{phrases.id}</td>
                                                <td>{phrases.phrase}</td>
                                                <td><Delete phrases_t={phrases} id={phrases.id} phrase={phrases.phrase}/></td>
                                        </tr>
                             })}   
                             </tbody>
                             </table>
                         </div>
                    </div>
                    <button className="btn btn-success" onClick={this.openPopup}>Add Phrase</button>

                      <Popup show={this.state.isOpen}
                        onClose={this.closePopup}>
                            <Edit Phrase={this.props.Phrase}></Edit>
                      </Popup>
                   </div>
               )}
           </AppContextConsumer>
             )    
        }
    }