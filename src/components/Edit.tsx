import * as React from 'react';
import { Phrase } from '../model/phrase.model';
import { PhraseService } from '../service/phrase.service';
import { AppContextInterface } from '../service/test.service';

interface PhraseProps{
    currentPhrase:string;
    phrases:Array<string>;
}

interface iProp{
    Phrase:PhraseService
}
 
export class Edit extends React.Component<iProp,PhraseProps>{

    constructor(props1:iProp){
        super(props1);
        this.state = {
            currentPhrase:"",
            phrases:[],
        }
    }

    handleSubmit(e: any){
        e.preventDefault();
        this.setState({
            currentPhrase: "",
            phrases:[
                ...this.state.phrases,
                this.state.currentPhrase
            ]
        })
        console.log(this.props.Phrase.getPhrases());
        this.props.Phrase.addPhrases({id:this.props.Phrase.getLength()+1,phrase:this.state.currentPhrase});
    }


    render(){
        return(
            
            <div className="row" onSubmit={(e) => this.handleSubmit(e)}>
                <div className="col-xs-10 col-xs-offset-1">
                <form>
                    <div className="form-group">
                        <label>Phrase</label>
                        <input type="text" className="form-control" placeholder="Phrase"
                        value={this.state.currentPhrase}
                        onChange = {(e) => this.setState({currentPhrase: e.target.value})}/>
                    </div>
                   
                    <button type="submit" className="btn btn-default">Add</button>
                </form>
            </div>
        </div>  

        )
    }
}