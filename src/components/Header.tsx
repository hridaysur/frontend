import * as React from 'react';
import {Link} from 'react-router-dom';

export const Header = () => {
        return(
            <nav className="navbar navbar-default">
                <div className="container">
                    <div className="navbar-header">
                        <ul className="nav navbar-nav">
                            <li><a href="/">Home</a></li>
                            <li><a href="/add">Add Phrase</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
}
