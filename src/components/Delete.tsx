import * as React from 'react';
import { Phrase } from '../model/phrase.model';
import { PhraseService } from '../service/phrase.service';

interface Iprop {
    id:number,
    phrase:string
    phrases_t:Phrase
}
export class Delete extends React.Component<Iprop>{

    deleteItem = (e:any) => {
        console.log(e);
    }
    render(){
        return(
            <button className="btn btn-danger" value={this.props.id -1} onClick = {(e) => {this.deleteItem(e.currentTarget.value)}}>Delete</button>
        )
    }
}