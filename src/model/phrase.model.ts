export class Phrase {
    public id : number;
    public phrase : string; 

    constructor(id:number , phrase : string){
        this.id = id;
        this.phrase = phrase;
    }
}